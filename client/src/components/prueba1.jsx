import {useHistory} from 'react-router-dom'
export default function PruebaA(){
    const {push}=useHistory()
    return(
    <div>
        <h1>Hola mundo!</h1>
        <button onClick={() => push("/thalia")}>Ir a</button>
    </div>)
}