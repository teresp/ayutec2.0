import {useHistory} from 'react-router-dom'
export default function PruebaB(){
    const {goBack}=useHistory()
    return(
    <div>
        <h1>Adiós mundo cruel!</h1>
        <button onClick={() => goBack()}>Regresar</button>
    </div>)
}