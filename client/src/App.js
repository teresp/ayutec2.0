import logo from './logo.svg';
import './App.css';
import Chayanne from './components/prueba1';
import Thalia from './components/prueba2';
import Navbar from './components/navBar';

import { BrowserRouter as Router, Route, Switch } from "react-router-dom"; 

function App() {
  return (
    <Router>
      <Navbar donsatur="don satur"/>
      <Switch>
        <Route exact path="/" render={() => <Chayanne/>}/>
        <Route path="/thalia" render={() => <Thalia/>}/>
      </Switch>
    </Router>
  );
}

export default App;
