const http = require('http');
const fs = require('fs').promises;
/* const body = require("./index.ejs"); */

http.createServer((req, res) =>{
    fs.readFile(__dirname + "/index.ejs")
    .then(contents => {
        res.setHeader("Content-Type", "text/html");
        res.writeHead(200);
        res.end(contents);
    })
    .catch(err => {
        res.writeHead(500);
        res.end(err);
        return;
    });
}).listen(3000);
console.log('Servidor iniciado...');
/* 
const requestListener = function (req, res) {          
};
 */
