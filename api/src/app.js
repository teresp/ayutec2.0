const express = require("express")
const routes = require("./routes/index.js") 
const bodyParser = require("body-parser") 
const morgan = require("morgan")
const server = express()

server.name = "API";
server.use(bodyParser.urlencoded({ extended: true, limit: "50mb" }));
server.use(bodyParser.json({ limit: "50mb" }));
server.use(bodyParser.json());
server.use(bodyParser.urlencoded({ extended: true }));
server.use(morgan("dev"));

server.use((err, req, res, next) => {
    const status = err.status || 500;
    const message = err.message || err;
    console.error(err);
    res.status(status).send(message);
  });
  
  server.use("/", routes);
  
  module.exports = server;