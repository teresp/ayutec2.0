const { DataTypes } = require("sequelize");
module.exports = (sequelize) => {
    const Cliente = sequelize.define('cliente', {
        nombre: {
            type: DataTypes.STRING,
            allowNull: false
          },
          apellido: {
            type: DataTypes.STRING,
            allowNull: false
          },
          telefono: {
            type: DataTypes.INTEGER(13),
            allowNull: false
          },
          direccion: {
            type: DataTypes.STRING,
            allowNull: false
          },
          observaciones: {
            type: DataTypes.TEXT("medium")
          }
    })
};