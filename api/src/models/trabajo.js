const { DataTypes } = require("sequelize");
module.exports = (sequelize) => {
    const Trabajo = sequelize.define('trabajo', {

        titulo: {
            type: DataTypes.STRING,
            allowNull: false
          },
          descripcion: {
            type: DataTypes.TEXT,
            allowNull: false
          },
          imagen: {
            type: DataTypes.STRING
          },
          estado: {
            type: DataTypes.BOOLEAN
          },
          presupuesto: {
            type: DataTypes.FLOAT
          },
          fechaEntrega: {
            type: DataTypes.DATE
          }
          
    })
};