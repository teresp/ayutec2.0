const { DataTypes } = require("sequelize");

module.exports = (sequelize) => {
  const Tecnico = sequelize.define("Tecnico", {
    Nombre: {
      type: DataTypes.STRING,
      allowNull: false
    },
    Apellido: {
      type: DataTypes.STRING,
      allowNull: false
    },
    Correo: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    Clave: {
      type: DataTypes.STRING,
      allowNull: false,
    },
  });
};